#Importar librerias
import cv2

#Se crea la variable donde estara almacenado el video
cap = cv2.VideoCapture('Videos/vtest.avi')
#Instancia que contiene los metodos para la sustracción de fondo
fgbg = cv2.createBackgroundSubtractorMOG2()

#Ciclo while que se utilizara para leer los frames del video
while True:
    #Se establecen dos variables, ambas leeran el contenido de cap, o mejor dicho leeran el video
    #Ret se utilizara para comparación, en caso de ser falso el programa termina
    #Frame sera utilizado para la sustraccion de fondo
    ret, frame = cap.read()
    if ret == False: break
    #Se aplica la sustraccion de fondo previamente instaciada
    fgmask = fgbg.apply(frame)
    
    #Se crean 2 ventanas, la primera muestra el video con la sustraccion de fondo, el segundo muestra el video original
    cv2.imshow('fgmask',fgmask)
    cv2.imshow('frame',frame)
    
    #El proposito de waitKey es para recorrer el video a 30ms por frame, mientras que el & 0xFF se utilizada para leer el teclado en lenguaje ASCII, en caso de que se presione la tecla escape (ASCII =27) se cerraran las ventanas y terminara los videos
    k = cv2.waitKey(30) & 0xFF
    if k == 27:
        break

#Una vez terminado de reproducir los videos, se cerraran y terminara el programa
cap.release()
cv2.destroyAllWindows()
