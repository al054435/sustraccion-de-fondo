# Sustracción de Fondo

Proyecto realizado para la materia de Inteligencia Artificial aplicando los conocimientos vistos en el semestre

**Descripcion del proyecto**

_Este proyecto consiste en usar la sustracción de fondo, en este caso usando videos simulando el uso de camaras de seguridad._

La sustracción de fondo es una técnica ampliamente utilizada para detectar objetos en movimiento a partir de cámaras estáticas.
En el campo de la vídeo-vigilancia, es indispensable ya que permite discriminar rápidamente cuando ha surgido un evento y realizar estudios de movilidad de los objetos en escena. 

**Librerias utilizadas**

Cv2 (OpenCv)

**Instrucciones**

1.-Descargar el proyecto

2.-Ejecutar el programa atravez de una terminal de su preferencia

3.-En caso de que desee probar con mas videos, puede cambiar el nombre del video que este en el programa por otro de su preferencia, o poner mas videos, si ese es el caso, agregar los videos a la carpeta > Videos

**Integrantes del equipo**

Capriel Grabiel Carlos Alfredo

Castillo Sonda Fernando Daniel

Martinez Gomez Sarahi del Jesus
